<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Player controller.
 *
 */
class PlayerController extends Controller
{
    /**
     * Lists all player entities.
     * @Get(
     *     path = "player",
     *       name="player_index" )
     * @View
     *
     */
    public function indexAction()
    {
        $datas = $this->get('app.sports')->getAllLeagues();
        $leagues = [];
        foreach ($datas["leagues"] as $league) {
            $leagues[] = (object) $league;
        }

        return $this->render('player/index.html.twig', array(
            'leagues' => $leagues,
        ));
    }

    /**
     * Finds and displays a player entity.
     * @Get(
     *     path = "player/{id}/show",
     *     name="player_show" ,
     *     requirements = {"id"="\d+"}
     *     )
     * @View
     * @return
     */
    public function showAction()
    {
        $datas = $this->get('app.sports')->getChampionnat();
        $res = [];
        foreach ($datas["player"] as $data) {
                $res[] = (object) $data;
        }

        return $this->render('player/show.html.twig', array(
            'players' => $res,
        ));

    }

    /**
     * @Get(
     *     path = "player/{team}/team",
     *     name="players_team"
     *      )
     *@View
     */
    public function playersByTeamAction($team)
    {

        $leagues = $this->get('app.sports')->getAllLeagues();

        $resLeagues = [];
        foreach ($leagues["leagues"] as $league) {
            $resLeagues[] = (object) $league;
        }

        $datas = $this->get('app.sports')->getPlayersByTeam($team);
        $res = [];
        foreach ($datas["player"] as $data) {
            $res[] = (object) $data;
        }
        return $this->render('player/players_team.html.twig', array(
            'leagues' => $resLeagues,
            'players' => $res,
        ));
    }
}
