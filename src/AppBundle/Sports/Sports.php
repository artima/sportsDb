<?php
/**
 * Created by PhpStorm.
 * User: mizadi
 * Date: 30/08/2018
 * Time: 13:25
 */

namespace AppBundle\Sports;


use GuzzleHttp\Client;
use JMS\Serializer\Serializer;

class Sports
{
    private $sportsClient;
    private $apiKey;
    Private $serializer;

    /**
     * Sports constructor.
     * @param $sportsClient
     * @param $apiKey
     * @param $serializer
     */
    public function __construct(Client $sportsClient, $apiKey,Serializer $serializer)
    {
        $this->sportsClient = $sportsClient;
        $this->apiKey = $apiKey;
        $this->serializer = $serializer;
    }

    public function getChampionnat()
    {
        $uri = 'searchplayers.php?t=Arsenal';
        $response = $this->sportsClient->get($uri);

        $data = $this->serializer->deserialize(
            $response->getBody()->getContents(),
            'array',
            'json'
        );
        return $data;
    }

    public function getAllLeagues()
    {
        $uri = 'all_leagues.php';
        $response = $this->sportsClient->get($uri);

        $data = $this->serializer->deserialize(
            $response->getBody()->getContents(),
            'array',
            'json'
        );
        return $data;
    }

    public function getPlayersByTeam($idTeam)
    {
        $uri = 'lookup_all_players.php?id='.$idTeam;
        $response = $this->sportsClient->get($uri);

        $data = $this->serializer->deserialize(
            $response->getBody()->getContents(),
            'array',
            'json'
        );
        return $data;
    }
}